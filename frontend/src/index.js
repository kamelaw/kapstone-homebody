import React from "react";
import ReactDOM from "react-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import { App } from "./App";
import { AuthProvider } from "./components/contexts/AuthContext";

ReactDOM.render(
  //Authprovider right below strictMode
  <React.StrictMode>
    <AuthProvider>
      <App />
    </AuthProvider>
  </React.StrictMode>,
  document.getElementById("root")
);
