import * as firebase from 'firebase/app';
import 'firebase/auth'
import 'firebase/analytics'
import 'firebase/firestore'
import 'firebase/functions'



// Your web app's Firebase configuration
  // For Firebase JS SDK v7.20.0 and later, measurementId is optional
  const app = firebase.initializeApp ({

    apiKey: "AIzaSyDALoQP2xWCWNmjTu0Sd-Z2Q8q3XD9FZRg",
    authDomain: "kapstone-13bac.firebaseapp.com",
    databaseURL: "https://kapstone-13bac.firebaseio.com",
    projectId: "kapstone-13bac",
    storageBucket: "kapstone-13bac.appspot.com",
    messagingSenderId: "678214015282",
    appId: "1:678214015282:web:f4d792685e78b58f171f2b",
    measurementId: "G-7QJ06ZCW23"

  // apiKey: process.env.REACT_APP_FIREBASE_KEY,
  // authDomain: process.env.REACT_APP_FIREBASE_DOMAIN,
  // databaseURL: process.env.REACT_APP_FIREBASE_DATABASE,
  // projectId: process.env.REACT_APP_FIREBASE_PROJECT_ID,
  // storageBucket: process.env.REACT_APP_FIREBASE_STORAGE_BUCKET,
  // messagingSenderId: process.env.REACT_APP_FIREBASE_SENDER_ID,
  // appId: process.env.REACT_APP_FIREBASE_APP_ID,
  // measurementId: process.env.REACT_APP_FIREBASE_MEASUREMENT_ID
  });
  
  // Initialize Firebase
  firebase.analytics();
  const db = firebase.firestore();
  const FV = firebase.firestore.FieldValue.serverTimestamp()
  export { FV }
  export const functions = app.functions();
  export {db};
  export const auth =app.auth();
  export default app;

