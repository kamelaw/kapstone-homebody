import React from "react";
import { AuthProvider } from "../contexts/AuthContext";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import {
  SignupScreen,
  CommunityScreen,
  LoginScreen,
  PickPageScreen,
  AccountSettingScreen,
  ProfileScreen,
  NotFoundScreen,
} from "../../screens";
import { ProtectedRoute } from "../protectedRoute/ProtectedRoute";

export const Navigation = () => (
  <BrowserRouter>
    <AuthProvider />
    <Switch>
      <Route exact path="/" component={LoginScreen} />
      <Route exact path="/signup" component={SignupScreen} />
      <ProtectedRoute
        exact
        path="/community"
        isProtected
        component={CommunityScreen}
      />
      <ProtectedRoute
        exact
        path="/pickpage"
        isProtected
        component={PickPageScreen}
      />
      <ProtectedRoute
        exact
        path="/profile"
        isProtected
        component={ProfileScreen}
      />

      <Route
        exact
        path="/account-setting"
        isProtected
        component={AccountSettingScreen}
      />
      <ProtectedRoute exact path="*" component={NotFoundScreen} />
    </Switch>
    <AuthProvider />
  </BrowserRouter>
);
