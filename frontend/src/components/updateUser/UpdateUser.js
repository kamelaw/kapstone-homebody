import React, { useRef, useState } from "react";
import { Form, Button, Card, Alert, Container } from "react-bootstrap";
import { useAuth } from "../contexts/AuthContext";
import { Link, useHistory } from "react-router-dom";
import { db } from "../../firebase";
import "firebase/firestore";
import "./updateUser.css"

export const UpdateUser = () => {
  const usernameRef = useRef();
  const emailRef = useRef();
  const passwordRef = useRef();
  const passwordConfirmRef = useRef();
  const { currentUser, updatePassword, updateEmail, removeUser } = useAuth();
  const [error, setError] = useState();
  const [loading, setLoading] = useState(false);
  const history = useHistory();

  const handleDelete = async (event) => {
    event.preventDefault();
    try {
      setError("");
      setLoading(true);
      await removeUser().then(
        db.collection("user").doc(currentUser.uid).delete()
      );
      alert("Your account has been deleted, Goodbye");
      history.push("/");
    } catch {
      setLoading(false);
      setError("Failed to delete");
    }
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    if (passwordRef.current.value !== passwordConfirmRef.current.value) {
      return setError("Password does not match");
    }

    const promises = [];

    setLoading(true);
    setError("");

    if (emailRef.current.value !== currentUser.email) {
      promises.push(updateEmail(emailRef.current.value));
    }

    if (passwordRef.current.value) {
      promises.push(updatePassword(passwordRef.current.value));
    }

    Promise.all(promises)
      .then(() => {
        db.collection("user")
          .doc(currentUser.uid)
          .update({
            email: emailRef.current.value,
            password: passwordConfirmRef.current.value,
          })
          .then(() => {
            alert("Your account has been updated");
            history.push("/profile");
          });
      })
      .catch(() => {
        setError("Failed to update user");
      })
      .finally(() => {
        setLoading(false);
      });
  };
  return (
    <>
      <div className="update-form-card" >
      <Card>
        <Card.Body>
          <h4 className="update-header"> Update Profile</h4>
          {error && <Alert variant="danger">{error}</Alert>}
          <Form onSubmit={handleSubmit}>
            <Form.Group id="email">
              <Form.Label className="update-labels">Email</Form.Label>
              <Form.Control
                type="email"
                ref={emailRef}
                required
                defaultValue={currentUser.email}
              />
            </Form.Group>
            <Form.Group id="password">
              <Form.Label className="update-labels">Password</Form.Label>
              <Form.Control
                type="password"
                ref={passwordRef}
                required
                placeholder="password"
              />
            </Form.Group>
            <Form.Group id="password-confirm">
              <Form.Label className="update-labels">Password Confirmation</Form.Label>
              <Form.Control
                type="password"
                ref={passwordConfirmRef}
                placeholder="confirm password"
              />
            </Form.Group>
            <Button className="update-btn" disabled={loading} type="submit">
              Update
            </Button>

            <Button className="delete-btn" onClick={handleDelete}>Delete</Button>
          </Form>
        </Card.Body>
      </Card>
      </div>
      <div className="w-100 text-center mt-2">
        <Link to="/profile">Cancel</Link>
      </div>
    </>
  );
};