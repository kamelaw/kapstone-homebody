import React from "react";
import { useAuth } from "../contexts/AuthContext";
import { LandingPageContainer } from "../../components";
import { LoggedInMenuBarContainer } from "../../components";

export const Menu = () => {
  const { currentUser } = useAuth();

  return currentUser ? <LoggedInMenuBarContainer /> : <LandingPageContainer />;
};
