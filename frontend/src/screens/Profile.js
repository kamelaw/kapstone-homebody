import React, { useState } from "react";
import { Alert } from "react-bootstrap";
import { MenuContainer } from "../components";
import { useAuth } from "../components/contexts/AuthContext";
import { Link } from "react-router-dom";

export const ProfileScreen = () => {
  const [error, setError] = useState("");
  const { currentUser } = useAuth();
    
  return (
    <>
      <MenuContainer />
      <div id="profile-wrap">
        {error && <Alert variant="danger">{error}</Alert>}
        <strong id="email-tag">Email: </strong>
        {currentUser.email}
        <br />
        <br />
        <Link to="/account-setting" className="btn btn-secondary w-20 mt-3">
          Account Settings
        </Link>
      </div>
    </>
  );
};
