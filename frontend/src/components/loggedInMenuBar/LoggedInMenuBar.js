import React, { useState } from "react";
import { NavLink, Link, useHistory } from "react-router-dom";
import { Navbar } from "react-bootstrap";
import { useAuth } from "../contexts/AuthContext";
import "./loggedInMenuBar.css";

export const LoggedInMenuBar = () => {
  const [error, setError] = useState();
  const { logout } = useAuth();
  const history = useHistory();
  async function handleLogout() {
    setError("");

    try {
      await logout();
      history.push("/");
    } catch {
      setError("Logout Failed");
    }
  }

  return (
    <>
    <h2 id="masthead-logged-in">HOMEBODY</h2>
    <Navbar id="nav-bar">
      <div>
        <div id="menu-links">
          <>
            <NavLink className="link" activeClassName="selected" to="/profile">
              Profile
            </NavLink>
            <NavLink className="link" activeClassName="selected" to="/community">
              Community
            </NavLink>
            <NavLink className="link" activeClassName="selected" to="/pickpage">
              Fit Pick
            </NavLink>
            <Link
              className="link"
              to="/"
              onClick={handleLogout}
            >
              Logout
            </Link>
          </>
        </div>
      </div>
    </Navbar>
    </>
  );
};
