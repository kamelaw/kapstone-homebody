import React from "react";
import { SignupContainer, MenuContainer } from "../components";
import { Container } from "react-bootstrap";
import { useAuth } from "../components/contexts/AuthContext";
import { Redirect } from "react-router-dom";

export const SignupScreen = () => {
  const { currentUser } = useAuth();
  return (
    <>
      {currentUser ? <Redirect to="/profile"/>:
<div>    
  <MenuContainer />
    <Container
      className="d-flex align-items-center justify-content-center"
      style={{ minHeight: "100vh" }}>
      <div className="w-300" style={{ maxWidth: "400px" }}>
        <SignupContainer />
      </div>
    </Container> 
    </div> 
    }
    </>

  )
};
