import React from "react";
import { useAuth } from "../contexts/AuthContext";
import "./message.css";

export const Message = (props) => {
  const { currentUser } = useAuth();
  const { text, user } = props.message;

  const messageSender = user === currentUser.email ? "sent" : "received";

  return (
    <>
      {/* <div id="message-container"> */}
        <div className={`message-${messageSender}`}>
          {/* <p id="message-body"> */}
          <p className= "text">  
           <span className="sent-message">{text} </span> 
           <br/>
            <span className="from-sender">from {user}  </span>
           
          </p>
        {/* </div> */}
      </div>
    </>
  );
};
