import React, { useRef, useState } from "react";
import { useCollectionData } from "react-firebase-hooks/firestore";
import { db, FV } from "../../firebase";
import { useAuth } from "../contexts/AuthContext";
import { MessageContainer } from "../../components";
import "./communityChat.css";
import { Form, Button } from "react-bootstrap";

export const CommunityChat = () => {
  const { currentUser } = useAuth();
  const user = useRef();
  const messagesRef = db.collection("messages");
  const query = messagesRef.orderBy("createdAt").limit(25);
  const [messages] = useCollectionData(query, { idField: "id" });
  const [formInput, setFormInput] = useState("");

  const sendMessage = async (event) => {
    event.preventDefault();
    const { uid } = currentUser;

    await messagesRef.add({
      user: currentUser.email,
      text: formInput,
      createdAt: FV,
      uid,
    });

    setFormInput("");
    user.current.scrollIntoView({ behavior: "smooth" });
  };

  return (
    <>
      <div id="chat-flex-parent">
        <div id="chat-container">
          <main id="message-feed">
            {messages &&
              messages.map((msg) => (
                <MessageContainer key={msg.id} message={msg} />
              ))}
            <span ref={user}></span>
          </main>
          <Form id="chat-input" onSubmit={sendMessage}>
            <input
              id="chat-input-field"
              value={formInput}
              onChange={(e) => setFormInput(e.target.value)}
              placeholder="say something nice to the world"
            />
            <Button id="chat-send-btn" type="submit" disabled={!formInput}>
              send 🏃‍♀️ 🏃 
            </Button>
           
          </Form>
        </div>
      </div>
    </>
  );
};