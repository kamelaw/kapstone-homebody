import React from "react";
import { MenuContainer, UpdateUserContainer } from "../components";

export const AccountSettingScreen = () => {
  return (
    <div>
      <MenuContainer />
      <UpdateUserContainer />
    </div>
  );
};
