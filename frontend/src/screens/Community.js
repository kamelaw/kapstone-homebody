import React from "react";
import { MenuContainer, CommunityChatContainer } from "../components";
import cheetahprint from "../assets/cheetahBackground.jpg";
import "./screens.css";

export const CommunityScreen = () => {
  return (
    <>
      <MenuContainer />
      <div
        style={{
          backgroundImage: `url(${cheetahprint})`,
          backgroundSize: "cover",
        }}
      >
        <h1 id="community-header">🧘🏽Lets Get Chatty!🏋🏿‍♂️</h1>

        <CommunityChatContainer />
      </div>
    </>
  );
};
