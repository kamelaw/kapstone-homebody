import React from "react";
import { MenuContainer, YoutubeContainer } from "../components";
import "../screens"
export const PickPageScreen = () => {
  return (
    <div>
      <MenuContainer />
      <div className="YT-videos">

      <YoutubeContainer searchTerm="Cardio" title="Cardio Workout" />
      <YoutubeContainer
        searchTerm="Prancercise workout"
        title="Fancy Prancing"
      />
      <YoutubeContainer searchTerm="Yoga" title="Yoga Fire!" />
      </div>
    </div>
  );
};
