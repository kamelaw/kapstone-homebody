import React, { useRef, useState } from "react";
import { Form, Button, Card, Alert } from "react-bootstrap";
import { useAuth } from "../contexts/AuthContext";
import { Link, useHistory } from "react-router-dom";
import "firebase/firestore";
import cheetahprint from "../../assets/cheetahBackground.jpg";
import "./signup.css";

export const Signup = () => {
  const emailRef = useRef();
  const passwordRef = useRef();
  const passwordConfirmRef = useRef();
  const { signup } = useAuth();
  const [error, setError] = useState();
  const [loading, setLoading] = useState(false);
  const history = useHistory();

  const handleSubmit = async (event) => {
    event.preventDefault();

    if (passwordRef.current.value !== passwordConfirmRef.current.value) {
      return setError("Password doesnt match");
    }

    try {
      setError("");
      setLoading(true);
      await signup(emailRef.current.value, passwordRef.current.value);
      alert("Your account has been created");
      history.push("/profile");
    } catch {
      setError("Failed to create an account");
      setLoading(false);
    }
  };

  return (
    <>
      <div id="login-card">
        <Card>
          <Card.Body
            style={{
              backgroundImage: `url(${cheetahprint})`,
              backgroundSize: "cover",
            }}
          >
            <h2 className="text-center mb-4"> SIGN UP</h2>

            {error && <Alert variant="danger">{error}</Alert>}
            <Form onSubmit={handleSubmit}>
              <Form.Group id="email">
                <Form.Label className="email">Email</Form.Label>
                <Form.Control
                  style={{ backgroundColor: "#e9ecef" }}
                  type="email"
                  ref={emailRef}
                  required
                />
              </Form.Group>
              <Form.Group id="password">
                <Form.Label className="password">Password</Form.Label>
                <Form.Control
                  style={{ backgroundColor: "#e9ecef" }}
                  type="password"
                  ref={passwordRef}
                  required
                />
              </Form.Group>
              <Form.Group id="password-confirm">
                <Form.Label className="password-confirm">
                  Password Confirmation
                </Form.Label>
                <Form.Control
                  style={{ backgroundColor: "#e9ecef" }}
                  type="password"
                  ref={passwordConfirmRef}
                  required
                />
              </Form.Group>
              <Button id="signup-btn" disabled={loading} type="submit">
                Sign Up
              </Button>
            </Form>
          </Card.Body>
        </Card>
        <div className="w-100 text-center mt-2">
          Already have and account? <Link to="/">Login</Link>
        </div>
      </div>
    </>
  );
};

